# Para rodar o projeto

* 1 - npm install
* 2 - npm start
* 3 - projeto abrirá em http://localhost:3000
* Projeto ainda em desenvolvimento. A rota inicial abre o componente referente aos produtos, filtrados por coleção couro
* A rota /produto abre a página do produto selecionado (em desenvolvimento)


# Desafio FrontEnd

## Instruções para o teste
* Dê um fork neste projeto;
* Desenvolva as telas;
* Atualize o readme com as instruções necessárias para rodar o seu código;
* Faça um pull request.


##### Sugestões de implementação
* Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)
* Filtro de produtos funcional
* Adicionar produtos ao carrinho
* Botão de carregar mais produtos

##### Dicas
* Evite usar linguagens, ferramentas e metodologias que não domine;
* Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;

##### Link para o layout
* Mobile: https://xd.adobe.com/view/e372993c-5e75-4462-91a4-1982b31e5726-b621/ 
* Desktop: https://xd.adobe.com/view/2c1dace9-52a9-4416-bb15-18a2f9e32059-794e/



###### Dúvidas: vinicius.diniz@somagrupo.com.br


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
