import React from 'react'
import { Provider } from 'react-redux'
import { configureStore } from '../store'
import { BrowserRouter as Router } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/header'
import Main from '../components/main/main'
import { createBrowserHistory } from "history";

const customHistory = createBrowserHistory();

const store = configureStore()

const App = () => (
  <Provider store={store}>
    <Router>
      <div>
        <Header history={customHistory}/>
      </div>
      <div>
        <Main history={customHistory}/>
      </div>
    </Router>
  </Provider>
)

export default App;
