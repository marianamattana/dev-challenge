import React, { Component, Fragment } from 'react'
import { 
  Navbar,
  Nav,
} from 'react-bootstrap'
import Collection from '../main/collection/collection'
// styles
import './header.scss'

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
    
    this.handleCollectionClick = this.handleCollectionClick.bind(this)

  }

  handleCollectionClick() {
    this.setState({
      open: !this.state.open,
    })
  }

  render(){
    return (
      <Fragment>
        <Navbar bg="light" expand="lg" className="mr-auto justify-content-center navbar-header" fixed="top">
          <Nav className="justify-content-start">
            <Navbar.Brand href="https://www.animale.com.br/nossas-lojas"><img src="map.png" alt="Nossas Lojas" className="header"/></Navbar.Brand>
            <Navbar.Brand href="https://api.whatsapp.com/send?phone=552137337702"><img src="phone.png" alt="Phone" className="header"/></Navbar.Brand>
          </Nav>
          <Nav className="flex-column">
            <Navbar.Brand href="#home" className="brand align-self-center">ANIMALE</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-between">
              <Nav className="justify-content-center">
                <Nav.Link href="#novidades" className="header">NOVIDADES</Nav.Link>
                <Nav.Link href="#" className="header" onClick={this.handleCollectionClick}>COLEÇÃO</Nav.Link>
                <Nav.Link href="#joias" className="header">JOIAS</Nav.Link>
                <Nav.Link href="#sale" className="header">SALE</Nav.Link>
                <Nav.Link href="#inside" className="header"><img src="inside.png" alt="inside" className="header"/></Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Nav>
          <Nav className="d-flex justify-content-end">
              <Navbar.Brand href="#"><img src="user.png" alt="Login" className="header"/></Navbar.Brand>
              <Navbar.Brand href="#"><img src="bag.png" alt="Sacola de compras" className="header"/></Navbar.Brand>
              <Navbar.Brand href="#"><img src="search.png" alt="Busca" className="header"/></Navbar.Brand>
          </Nav>
        </Navbar>
        {this.state.open && (<Collection/>)}
      </Fragment>
    )
  }
}

export default Header