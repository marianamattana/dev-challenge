import React from 'react'
import { Switch, Route, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Products from './products/products'
import Product from './products/product'
import './main.scss'

const Main = props => {
  return(
    <div>
      <Switch>
        <Route
          path="/"
          render={props => <Products {...props} />}
        />
        <Route
          path="/produto"
          render={props => <Product {...props} />}
        />
      </Switch>
    </div>
  )
}

export default withRouter(
  Main
)