import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'
import { fetchCollections } from '../../../store/actions/collections'
import './productFilter.scss'

class ProductFilter extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount() {
    this.props.fetchCollections()
  }

  handleClick= e => {
    const selectedItem = e.target.id
    console.log(selectedItem)
    // this.props.history.push(`${selectedItem}`)
  }

  render(){
    const { collectionList } = this.props
    return(
      <div class="container">
        {!isEmpty(collectionList) && (
            <ul>
              {Object.values(collectionList).map(key => <li > {Object.keys(key)}
                <ul>
                  {Object.values(key).map( item => item.map( i => <li href="#" onClick={this.handleClick} id={"/categorias/".concat(i)}>{i}</li>))}
                </ul>
              </li>)}
          </ul>
        )}
      </div>
    ) 
  }
}

function mapStateToProps(state){
  return {
    collectionList: state.collectionList
  }
}

export default connect(mapStateToProps, { fetchCollections }) (ProductFilter)