import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEmpty, isNil } from 'ramda'
import ProductCardComponent from './card'
import ProductFilter from './productFilter'
import ProductOrderBy from './productOrderBy'
import { fetchProducts } from '../../../store/actions/products'
import './products.scss'

class Products extends Component {

  constructor(props) {
    super(props)
    this.state = {
      openFilter: false,
      openOrderBy: false,
    }

    this.handleClick = this.handleClick.bind(this)
    this.handleFilterClick = this.handleFilterClick.bind(this)
    this.handleOrderByClick = this.handleOrderByClick.bind(this)
  }

  componentDidMount() {
    this.props.fetchProducts()
  }

  handleClick = e => {
    e.preventDefault();
    const clothing = this.props.location.pathname
    console.log(clothing)
    // this.props.history.push("/couro")
  }

  handleFilterClick() {
    this.setState({
      openFilter: !this.state.openFilter,
    })
  }

  handleOrderByClick() {
    this.setState({
      openOrderBy: !this.state.openOrderBy,
    })
  }

  render(){
    const { productList } = this.props
    const list = productList.map(item => ({
      productName:item.productName,
      image: item.items[0].images[0].imageUrl,
      price: item.items[0].sellers[0].commertialOffer.Price,
      installments:
        (item.items[0].sellers[0].commertialOffer.Price !== 0 ?
        (Number(item.items[0].sellers[0].commertialOffer.Price)/10).toLocaleString('pt-BR', {minimumFractionDigits: 2}) : 0),
      addToCartLink: item.items[0].sellers[0].addToCartLink
    }))
    return(
      <div>
        <div class="row chosen-collection-row">
          <div class="col-lg-7 div-texts">
            <div class="container">
              <h1>COURO</h1>
              <p>
                As peças de couro da ANIMALE são irreverentes e se destacam nas produções.
                Os shapes assimétricos, detalhes metálicos e as tonalidades mais escuras e vibrantes imprimem sofisticação na hora de compor um look.
                Renove seu closet para as próximas estações com as roupas de couro femininas da nossa coleção.
              </p>
              <div class="row row-buttons">
                <button id="vestido" onClick={this.handleClick}>VESTIDOS DE COURO</button>
                <button id="calca" onClick={this.handleClick}>CALÇAS DE COURO</button>
                <button id="jaquetas" onClick={this.handleClick}>JAQUETAS DE COURO</button>
                <button id="sale" onClick={this.handleClick}>COURO EM SALE</button>
              </div>
            </div>
          </div>
          <div class="col-lg-5 div-img">
            <img src="leather.png" alt="Leather Collection" class="chosen-collection-img"/>
          </div>
        </div>
        <div class="row display-collection-name-filter">
          <div class="col-lg-4 display-collection-name">
            <p>Coleção &#8227; Couro</p>
          </div>
          <div class="col-lg-5 row-filter-buttons">
            <button onClick={this.handleFilterClick}>FILTRAR POR</button>
            <button onClick={this.handleOrderByClick}>ORDENAR POR</button>
          </div>
        </div>
        {this.state.openFilter && (<ProductFilter/>)}
        {this.state.openOrderBy && (<ProductOrderBy/>)}
        <div class="row products-row">
            {(!isEmpty(list) || !isNil(list)) && (
              list.map(list => 
                <ProductCardComponent
                  productName={list.productName}
                  image={list.image}
                  price={list.price}
                  installments={list.installments}
                  addToCartLink={list.addToCartLink}
                  />
              )
            )}
        </div>
      </div>
    ) 
  }
}

function mapStateToProps(state){
  return {
    productList: state.productList
  }
}

export default connect(mapStateToProps, { fetchProducts }) (Products)