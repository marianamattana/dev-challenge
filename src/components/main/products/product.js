import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEmpty, isNil } from 'ramda'
import { Card, Button } from 'react-bootstrap'
import { fetchProducts } from '../../../store/actions/products'
import './product.scss'

class Product extends Component {

  constructor(props) {
    super(props)
    this.state = {
      openFilter: false,
      openOrderBy: false,
    }

    this.handleClick = this.handleClick.bind(this)
    this.handleFilterClick = this.handleFilterClick.bind(this)
    this.handleOrderByClick = this.handleOrderByClick.bind(this)
  }

  componentDidMount() {
    this.props.fetchProducts()
  }

  handleClick = e => {
    e.preventDefault();
    const clothing = this.props.location.pathname
    console.log(clothing)
    // this.props.history.push("/couro")
  }

  handleFilterClick() {
    this.setState({
      openFilter: !this.state.openFilter,
    })
  }

  handleOrderByClick() {
    this.setState({
      openOrderBy: !this.state.openOrderBy,
    })
  }

  render(){
    const { productList } = this.props
    let list = []
    let productName = ''
    let images = []
    let price = ''
    let installments = ''
    let addToCartLink = ''
    if (!isEmpty(productList) && !isNil(productList)) {
    list = productList[0]
    productName = list.productName
    images = list.items[0].images
    price = list.items[0].sellers[0].commertialOffer.Price
    installments =
          (list.items[0].sellers[0].commertialOffer.Price !== 0 ?
          (Number(list.items[0].sellers[0].commertialOffer.Price)/10).toLocaleString('pt-BR', {minimumFractionDigits: 2}) : 0)
    addToCartLink = list.items[0].sellers[0].addToCartLink
    }
    return(
      <div>
        <div class="row">
          <div class="col-lg-4 collection-name-filter">
            <p>Coleção &#8227; Couro</p>
          </div>
        </div>
        <div class="row products-row">
              <div class="product-card-div">
              {(!isEmpty(productList) && !isNil(productList)) && (
                <Card bsPrefix="product-card">
                  <div class="row">
                    <div class="row col-lg-8">
                      <div class="row">
                        {images.map( i =>
                          <div class="col-md-6 col-img">
                            <Card.Img variant="top" src={i.imageUrl} class="card-img"/>
                          </div>
                          )}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <Card.Body>
                        <Card.Text>{productName}</Card.Text>
                        <div class="row div-prices">
                          <Card.Title >R$ {price}</Card.Title>
                          <Card.Text bsPrefix="card-installments">10 x R$ {installments}</Card.Text>
                        </div>
                        <Button variant="outline-dark" size="sm" href={addToCartLink}>QUICK SHOP</Button>
                      </Card.Body>
                    </div>
                  </div>
                </Card>
              )}
              </div>
        </div>
      </div>
    )  
  }
}

function mapStateToProps(state){
  return {
    productList: state.productList
  }
}

export default connect(mapStateToProps, { fetchProducts }) (Product)