import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { isEmpty, isNil } from 'ramda'
import { CardGroup, Card, Button } from 'react-bootstrap'
import { fetchProducts } from '../../../store/actions/products'
import './card.scss'

class ProductCardComponent extends Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this)
 }


  handleClick(e) {
    e.preventDefault();
    this.props.history.push("/");
  }

  render(){
    const { price, productName, addToCartLink, image, installments } = this.props
    return(
      <div class="product-card-div col-xs-6 col-lg-3">
        <Card bsPrefix="product-card">
          <Card.Img variant="top" src={image} class="card-img"/>
          <Card.Body>
            <Card.Text>{productName}</Card.Text>
            <div class="row div-prices">
              <Card.Title >R$ {price}</Card.Title>
              <Card.Text bsPrefix="card-installments">10 x R$ {installments}</Card.Text>
            </div>
            <Button variant="outline-dark" size="sm" href={addToCartLink}>QUICK SHOP</Button>
          </Card.Body>
        </Card>
      </div>
    ) 
  }
}

function mapStateToProps(state){
  return {
    productList: state.productList
  }
}

export default connect(mapStateToProps, { fetchProducts }) (ProductCardComponent)