import { combineReducers } from "redux"
import basket from './basket'
import collectionList from './collection'
import productList from './products'

const rootReducer = combineReducers({
  basket,
  collectionList,
  productList
})

export default rootReducer
