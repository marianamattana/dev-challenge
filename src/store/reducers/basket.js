import { ADD_PRODUCT, REMOVE_PRODUCT } from '../acionTypes'

const initialState = {
  basketProducts: [],
  id: 0
}

export default (state = initialState, action) => {
  switch(action.type){
    case ADD_PRODUCT:
      let newBasketAdd = {...state}
      newBasketAdd.id++
      return{
        ...newBasketAdd,
        basketProducts: [...newBasketAdd.basketProducts, {product: action.product, id:newBasketAdd.id}]
      }
    case REMOVE_PRODUCT:
      let newBasketRemove = state.basketProducts.filter(val => val.id !== action.id)
      return{
        ...state, newBasketRemove
      }
    default:
      return state
  }
}