import { LOAD_COLLECTIONS } from '../acionTypes'

const initialState = []

export default (state = initialState, action) => {
  switch(action.type){
    case LOAD_COLLECTIONS:
      return [...action.collectionList]
    default:
      return state
  }
}