import { LOAD_PRODUCTS } from '../acionTypes'

const initialState = []

export default (state = initialState, action) => {
  switch(action.type){
    case LOAD_PRODUCTS:
      return [...action.productList]
    default:
      return state
  }
}