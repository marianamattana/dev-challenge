import { apiCall } from '../../services/api'
import { LOAD_PRODUCTS } from '../acionTypes'

export const loadProducts = (productList) => ({
  type: LOAD_PRODUCTS,
  productList
})

export const fetchProducts = () => {
  return dispatch => {
    return apiCall('/data/products.json')
      .then((res) => {
        dispatch(loadProducts(res.data))})
      .catch(function(err){
        return console.log(err) }
      )
  }
}