import { apiCall } from '../../services/api'
import { LOAD_COLLECTIONS } from '../acionTypes'

export const loadCollections = (collectionList) => ({
  type: LOAD_COLLECTIONS,
  collectionList
})

export const fetchCollections = () => {
  return dispatch => {
    return apiCall('/data/cattegoryList.json')
      .then((res) => {
        dispatch(loadCollections(res.data))})
      .catch(function(err){
        return console.log(err) }
      )
  }
}